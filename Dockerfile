FROM python:3.7.5-slim
WORKDIR /home
RUN python -m pip install DateTime
RUN apt-get update && apt-get install -y vim
CMD ["python", "/home/test.py"]